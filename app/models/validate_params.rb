class ValidateParams
  def initialize survivor_params, survivor
    @survivor_params = survivor_params
    @survivor = survivor
    @expected_params = ["id", "latitude", "longitude"]
  end

  def perform
    validate_location_params
  end

  def formated_params
    @survivor_params.to_hash.keys.each.map { |param| param.to_s }
  end

  def validate_location_params(check_params = 0)
    @survivor_params = formated_params
    @survivor_params.each do |param|
      if !@expected_params.include?(param)
        check_params += 1
      end
    end
    check_params == 0 && @survivor.uninfected?
  end
end