class ItemCounter
	def initialize(ids)
		@ids = ids
	end

	def average_resources
	survivors_size = @ids.size.to_f
	items_hash = { water: 0, food: 0, medication: 0, ammunition: 0 }
	items_from_model = Item.select(:item_name, :item_quantity, :survivor_id)
	@ids.each do |id|
		item_tmp = items_from_model.where(survivor_id: id)
		item_tmp.each do |item|
			item_name = item.item_name.upcase
			item_quantity = item.item_quantity
			case item_name
				when "WATER"
					items_hash[:water] += item_quantity
				when "FOOD"
					items_hash[:food] += item_quantity
				when "MEDICATION"
					items_hash[:medication] += item_quantity
				when "AMMUNITION"
					items_hash[:ammunition] += item_quantity
				end
			end
		end
	items_hash.transform_values! { |v| (v / survivors_size).round(2) }
	items_hash
	end

def points_lost
	puts @ids
	items_hash = { water: 0, food: 0, medication: 0, ammunition: 0 }
	items_from_model = Item.select(:item_name, :item_quantity, :survivor_id)
	@ids.each do |id|
		item_tmp = items_from_model.where(survivor_id: id)
		item_tmp.each do |item|
			item_name = item.item_name.upcase
			item_quantity = item.item_quantity
			case item_name
				when "WATER"
					items_hash[:water] += item_quantity * 4
				when "FOOD"
					items_hash[:food] += item_quantity * 3
				when "MEDICATION"
					items_hash[:medication] += item_quantity * 2
				when "AMMUNITION"
					items_hash[:ammunition] += item_quantity * 1
				end
			end
		end
	items_hash
	end
end