module Api
	module V1
		class TradesController < ApplicationController
			before_action :set_traders, :validate_params, :check_ifections
			before_action :both_equals, :check_if_points_are_equals

			def trade_item
				sender_id = @sender[:sender_id]
				receiver_id = @receiver[:receiver_id]
				sender_items = @sender["items"]
				receiver_items = @receiver["items"]
				status = StartTrading.new(sender_id,
																	sender_items,
																	receiver_id,
																	receiver_items).trade
				codes_msg = {"001":"Please, check the quantity",
										"002":"Please check if the item exists",
										"999":"The trading was successfully done!"}
				
				if status == "001" || status == "002"
					render json: { status: 422, error: {
						message: codes_msg[status.to_sym]
						}}, status: :unprocessable_entity
				else
					render json: { status: 200,
						message: codes_msg[status.to_sym]
						}, status: :ok
				end
			end

			private
			def set_traders
				@sender = params[:sender]
				@receiver = params[:receiver]
			end

			def check_if_points_are_equals
				@sender_points = TradeItems.new(@sender["items"]).count_items_points
				@receiver_points = TradeItems.new(@receiver["items"]).count_items_points
				render json: { status: 422, error: {
					message: "The points are not equals", 
					points: { 
						sender: @sender_points,
						receiver: @receiver_points
					} } }, status: :unprocessable_entity if @sender_points != @receiver_points
			end

			def both_equals
				equals = @sender[:sender_id] == @receiver[:receiver_id]
				render json: { status: 422, error: {
					message: "Both sender and receiver are the same survivor"
					}}, status: :unprocessable_entity if equals
			end

			def check_ifections
				both_uninfected = Survivor.find(@sender[:sender_id]).uninfected? && Survivor.find(@receiver[:receiver_id]).uninfected?
				render json: { status: 422, error: {
					message: "Both have to be uninfected to trade items"
					}}, status: :unprocessable_entity unless both_uninfected
			end

			def validate_params
				missing_params = false
				if @sender.nil? || @receiver.nil?
					missing_params = true
				else
					unless @sender.keys == %w[sender_id items] && 
						@receiver.keys == %w[receiver_id items]
						missing_params = true
					end
				end
				render json: { status: 400, error: {
					message: "Missing parameters"
					}}, status: :bad_request if missing_params
			end
		end
	end
end