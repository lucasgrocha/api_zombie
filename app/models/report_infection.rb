class ReportInfection
  def initialize(survivor)
    @survivor = survivor
  end

  def infect_survivor
    Survivor.where(id: @survivor.id).update(:infection_reports => @survivor.infection_reports + 1)
    current_survivor = Survivor.find(@survivor.id)
    if current_survivor.infection_reports == 3
      current_survivor.infected!
    end
  end
end