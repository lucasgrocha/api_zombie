require 'rails_helper'

RSpec.describe Survivor, type: :model do

  it 'returns the correct survivor title' do
    name = Faker::Name.first_name
    age = rand(1..99)
    infection_status = %i[infected uninfected].sample

    survivor = create(:survivor, name: name, age: age, infection_status: infection_status)
    expect(survivor.survivor_title).to eq("#{name} #{age} #{infection_status}")
  end

end