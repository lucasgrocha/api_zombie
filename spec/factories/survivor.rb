FactoryBot.define do 
  factory :survivor do
    name { Faker::Name.first_name }
    gender { %w[male female].sample }
    age { rand(1..99) }
    infection_status { %i[infected uninfected].sample }
    infection_reports { rand(0..3) }
    latitude { rand(1000..41235) }
    longitude { rand(1000..41235) }
  end
end