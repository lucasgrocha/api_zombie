class TradeItems
  def initialize items
    @items = items.dup
  end

  def count_items_points
    items_hash = @items.map { |i| {i["name"].to_sym => i["quantity"]} }
    items_counter = {food: 0, water: 0, ammunition: 0, medication: 0}
    items_hash.each do |items|
      items.each do |key, value|
        item_name = key.to_s
        case item_name
        when "water"
          items_counter[:water] += value * 4
        when "food"
          items_counter[:food] += value * 3
        when "medication"
          items_counter[:medication] += value * 2
        when "ammunition"
          items_counter[:ammunition] += value * 1
        end
      end
    end
    puts ''
    print items_counter
    puts ''
    points = items_counter.map { |k, v| v }.sum
    points
  end
end