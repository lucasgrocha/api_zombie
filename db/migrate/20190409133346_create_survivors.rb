class CreateSurvivors < ActiveRecord::Migration[5.2]
  def change
    create_table :survivors do |t|
      t.string :name
      t.integer :age
      t.string :gender
      t.decimal :latitude
      t.decimal :longitude
      t.integer :infection_status, default: 0
      t.integer :infection_reports, default: 0

      t.timestamps
    end
  end
end
