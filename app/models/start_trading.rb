class StartTrading
  def initialize(sender_id, sender_items, receiver_id, receiver_items)
    @sender_id = sender_id
    @sender_items = sender_items.dup.map { |i| {i["name"].to_sym => i["quantity"]} }
    @receiver_id = receiver_id
    @receiver_items = receiver_items.dup.map { |i| {i["name"].to_sym => i["quantity"]} }
  end

  def trade
    sender_items_db = Item.where(survivor_id: @sender_id).pluck(:item_name).dup
    receiver_items_db = Item.where(survivor_id: @receiver_id).pluck(:item_name).dup
    close_trade = false
    @sender_items.each do |item|
      item.each do |item_name, quantity|
        item_name = item_name.to_s
        if sender_items_db.include?(item_name)
          you_have = get_quantity_from_db(@sender_id, item_name)
          passed = get_quantity_passed(@sender_items, item_name)
          if passed > you_have || you_have == 0
            return "001"
          end
        else
          close_trade = true
        end
      end
    end
    return "002" if close_trade
    
    close_trade = false
    @receiver_items.each do |item|
      item.each do |item_name, quantity|
        item_name = item_name.to_s
        if receiver_items_db.include?(item_name)
          you_have = get_quantity_from_db(@receiver_id, item_name)
          passed = get_quantity_passed(@receiver_items, item_name)
          if passed > you_have || you_have == 0
            return "001"
          end
        else
          close_trade = true
        end
      end
    end
    return "002" if close_trade

    @receiver_items.each do |item|
      item.each do |name, quantity|
        name = name.to_s
        not_included = sender_items_db.include?(name)
        unless not_included
          Item.create!(item_name: name, item_quantity: 0, survivor_id: @sender_id)
          Item.where(item_name: name, survivor_id: @sender_id).update(item_quantity: quantity)
          qtd = Item.where(survivor_id: @receiver_id, item_name: name)[0].item_quantity
          Item.where(survivor_id: @receiver_id, item_name: name)[0].update(item_quantity: qtd - quantity)
        else
          qtd_sender = Item.where(survivor_id: @sender_id, item_name: name)[0].item_quantity
          Item.where(survivor_id: @sender_id, item_name: name)[0].update(item_quantity: qtd_sender + quantity)
          qtd = Item.where(survivor_id: @receiver_id, item_name: name)[0].item_quantity
          Item.where(survivor_id: @receiver_id, item_name: name)[0].update(item_quantity: qtd - quantity)
        end
      end
    end
    
    @sender_items.each do |item|
      item.each do |name, quantity|
        name = name.to_s
        not_included = receiver_items_db.include?(name)
        unless not_included
          Item.create!(item_name: name, item_quantity: 0, survivor_id: @receiver_id)
          Item.where(item_name: name, survivor_id: @receiver_id).update(item_quantity: quantity)
          qtd = Item.where(survivor_id: @sender_id, item_name: name)[0].item_quantity
          Item.where(survivor_id: @sender_id, item_name: name)[0].update(item_quantity: qtd - quantity)
        else
          qtd_receiver = Item.where(survivor_id: @receiver_id, item_name: name)[0].item_quantity
          Item.where(survivor_id: @receiver_id, item_name: name)[0].update(item_quantity: qtd_receiver + quantity)
          qtd = Item.where(survivor_id: @sender_id, item_name: name)[0].item_quantity
          Item.where(survivor_id: @sender_id, item_name: name)[0].update(item_quantity: qtd - quantity)
        end
      end
    end

    ids_to_destroy = Item.where(item_quantity: 0).pluck(:id)
    Item.destroy(ids_to_destroy)
    return "999"
  end

  private
  def get_quantity_from_db(survivor_id, item_name)
    Item.where(survivor_id: survivor_id, item_name: item_name)[0].item_quantity
  end

  def get_quantity_passed(survivor, item_name)
    survivor.map {|i| i[item_name.to_sym]}.compact[0]
  end
end