Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
    namespace :v1 do
      resources :survivors
      post '/survivor/add_survivor', to: 'survivors#add_survivor'
      put '/survivor/update_location', to: 'survivors#update_location'
      put '/survivor/report_infected', to: 'survivors#report_infected'
    end
  end

  namespace :api do
    namespace :v1 do
      resources :reports
      get '/report/infected_percentage', to: 'reports#infected_percentage'
      get '/report/uninfected_percentage', to: 'reports#uninfected_percentage'
      get '/report/points_lost', to: 'reports#points_lost'
      get '/report/average_resources', to: 'reports#average_resources'
    end
  end

  namespace :api do
    namespace :v1 do
      resources :trades
      put '/trade/trade_item', to: 'trades#trade_item'
    end
  end
end
