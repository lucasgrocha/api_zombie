class ItemManager
	def initialize(items, survivor)
			@items = items
			@survivor_id = survivor.id
		end
		
		def perform
			store_items
	end

	private
	def store_items
		@items.each do |item|
			item = item.to_hash
			Item.create!(item_name: item["item_name"], 
									item_quantity: item["item_quantity"], 
									survivor_id: @survivor_id)
		end
	end
end