TOTAL_PEOPLE_TO_GENERATE = 30

gender_options = ["male", "female"]
TOTAL_PEOPLE_TO_GENERATE.times do |i|
	gender = gender_options.at(rand(0..1))
	if gender == "male"
		name = "#{Faker::Name.male_first_name} #{Faker::Name.last_name}"
	else
		name = "#{Faker::Name.female_first_name} #{Faker::Name.last_name}"
	end
	age = rand(15..80)
	latitude = rand(1000.55..415145.99).round(2)
	longitude = rand(1000.55..4151.99).round(2)
	Survivor.create!(name: name, 
									age: age, gender: gender, 
									latitude: latitude, 
									longitude: longitude, 
									infection_status: 0, 
									infection_reports: 0)
end
puts "#{TOTAL_PEOPLE_TO_GENERATE} people was created!"

items = ["water", "food", "medication", "ammunition"]

TOTAL_PEOPLE_TO_GENERATE.times do |id|
Item.create!(item_name: items.at(rand(0..3)), 
							item_quantity: rand(1..10), 
							survivor_id: id + 1)
end

8.times do |i|
	Item.create!(item_name: items.at(rand(0..3)), 
							item_quantity: rand(1..10), 
							survivor_id: rand(1..TOTAL_PEOPLE_TO_GENERATE))
end

random_num_of_infections = rand(4..12)
loop do
	Survivor.find(rand(1..TOTAL_PEOPLE_TO_GENERATE)).infected!
	break if Survivor.select(:infection_status).where(infection_status: 1).size == random_num_of_infections
end

puts "#{random_num_of_infections} people got infected!"