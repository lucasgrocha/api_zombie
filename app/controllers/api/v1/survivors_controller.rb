module Api
  module V1
    class SurvivorsController < ApplicationController

      def index
        @survivors = []
        Survivor.all.size.times do |id|
          id = id + 1
          @survivors << {survivor:
            Survivor.select(Survivor.column_names - ['created_at', 'updated_at']).where(id: id), items: Item.select(:id, :item_name, :item_quantity).where(survivor_id: id)}
        end
        render json: @survivors
      end

      def add_survivor
        @survivor = Survivor.new(survivor_params)
        if @survivor.save
          ItemManager.new(item_params, @survivor).perform
          render json: { status: 200,
            message: "Survivor successfully created"
            }, status: :ok
        else
          render json: @survivor.errors, status: :unprocessable_entity
        end
      end
      
			def update_location
				survivor_id = survivor_params[:id]
				if Survivor.exists?(survivor_id)
					@survivor = Survivor.find(survivor_id)
					if valid_survivor(@survivor) && @survivor.update(survivor_params)
            render json: { status: 200,
              message: "Successfully updated"
              }, status: :ok
					else
              render_json_location_or_params_are_invalid
					end
				else
            render_json_survivor_not_found
				end
      end

      def report_infected
        set_report_params
        return if both_equals
        if both_exists
          delator = Survivor.find(@delator_id)
          survivor = Survivor.find(@survivor_id)
          if delator.infected?
						render json: { status: 422, error: {
							message: Messages.new.infected_cant_report
							}}, status: :unprocessable_entity
            return
          else
            if survivor.uninfected?
              ReportInfection.new(survivor).infect_survivor
            end
          end
        else
          render json: { status: 404, error: {
						message: Messages.new.survivor_and_delator_not_found
					}, available_survivors: Survivor.select(:id, :name, :infection_status) 
						}, status: :not_found
          return
        end
        json_render(Survivor.find(survivor.id), :ok)
      end

      private
      def render_json_location_or_params_are_invalid
        render json: { status: 422, error: {
          message: Messages.new.location_or_params_are_invalid
          }}, status: :unprocessable_entity
      end

      def render_json_survivor_not_found
        render json: { status: 404, error: {
          message: Messages.new.survivor_not_found
          }}, status: :not_found
      end

      def valid_survivor(survivor)
        ValidateParams.new(survivor_params, survivor).perform
      end

      def both_exists
        Survivor.exists?(@delator_id) && Survivor.exists?(@survivor_id)
      end

      def both_equals
        if @delator_id == @survivor_id
          render json: { status: 422, error: {
						message: Messages.new.self_report_not_available
						}}, status: :unprocessable_entity
          true
        else
          false
        end
      end

      def set_report_params
        @delator_id = params[:delator_id]
        @survivor_id = params[:survivor_id]
      end
      def json_render (msg, status)
        render json: msg, status: status
      end

      def survivor_params
        params.require(:survivor).permit(:id, :name, 
                                        :age,
                                        :gender, 
                                        :latitude,:longitude)
      end

      def item_params
        params.permit(item: [
          :item_name,
          :item_quantity]).require(:item)
      end
    end
  end
end
