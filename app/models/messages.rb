class Messages
  def location_or_params_are_invalid
    'Please check if the parameters are only id, latitude and longitude or if the survivor is infected.'
  end

  def survivor_not_found
    "The searched survivor does not exist."
  end

  def infected_cant_report
    "Infected cannot report"
  end

  def survivor_already_infected
    "The survivor reported is already infected"
  end

  def survivor_and_delator_not_found
    "Either user or survivor not found"
  end

  def self_report_not_available
    "Self report are not available"
  end
end