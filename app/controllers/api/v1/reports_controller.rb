module Api
  module V1
    class ReportsController < ApplicationController
      before_action :get_length_info_of_survivors
      
      def infected_percentage
        percentage = (@infections_length / @all_survivors_length) * 100
        percentage = percentage.round(2)
        render json: { status: 200, message: {percentage: percentage}
          }, status: :ok
      end

      def uninfected_percentage
        percentage = (@non_infections_length / @all_survivors_length) * 100
        percentage = percentage.round(2)
        render json: { status: 200, message: {percentage: percentage}
          }, status: :ok
      end

      def points_lost
        survivors_non_infected = Survivor.infected.ids
        items_quantity = ItemCounter.new(survivors_non_infected).points_lost
        render json: { status: 200, 
                      message: { 
                        "each item":items_quantity, 
                        total: items_quantity.values.sum}
                      }, status: :ok
      end

      def average_resources
				survivors_infected = Survivor.uninfected.ids
        items_quantity = ItemCounter.new(survivors_infected).average_resources
        render json: { status: 200,
                      message: {
                        "AVERAGE":items_quantity
                      }}, status: :ok
      end

      private
      def get_length_info_of_survivors
        @all_survivors_length = Survivor.all.size.to_f
        @infections_length = Survivor.infected.count.to_f
        @non_infections_length = @all_survivors_length - @infections_length
      end
    end
  end
end