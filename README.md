<img src="https://raw.githubusercontent.com/devlucasrocha/markdown_images/master/logo.png" width="800">

## Problem description
The world as we know it has fallen into an apocalyptic scenario. A laboratory-made virus is transforming human beings and animals into zombies, hungry for fresh flesh.

You, as a zombie resistance member (and the last survivor who knows how to code), was designated to develop a system to share resources between non-infected humans.
&nbsp;
&nbsp;

---

## Getting started

Firstly run this command:

``` git clone https://github.com/devlucasrocha/zssn_restAPI.git ```

Now run these commands in console: 

1) ``` cd zssn_restAPI/  ```

2) ``` bundle install ```

3) ``` rails db:create ```

4) ``` rails db:migrate ```

&nbsp;

If you prefer to get started with the database already filled with 30 survivors, you could run this:

``` rails db:seed ``` or ``` rails db:setup ```

&nbsp;

Thereafter you are able to use the API running the command below:

``` rails server ```

&nbsp;
&nbsp;

<p><a href="https://www.getpostman.com/collections/c1354fd8ed219dec83c4" target="_blank">Click here</a> if you would like to import the JSON requests to API already done in Postman.<p>
	
---

&nbsp;
&nbsp;

## In this project the following versions were used:

- Ruby 2.4.5
- Rails 5.2.3
- Bundler 2.0.1

If you ran into some problems about versions, you should change the version of ruby and rails in _Gemfile_ and also the version of bundler in _Gemfile.lock_ to the versions that are installed in your computer.

And thereafter run: ``` bundle install ```

&nbsp;
&nbsp;

---

# SURVIVORS CONTROLLER

## [Add a new survivor] - POST ```/api/v1/survivor/add_survivor```

 
 ###### headers
 ```Content-Type	application/json```
 
 ##### body
 
 ```JSON
 {
	"survivor": {
		"name": "Ana",
		"age": 41,
		"gender": "female",
		"latitude":-4123.3,
		"longitude":123135.41
	},
	"item": [
		{
		"item_name":"medication",
		"item_quantity":6
		},
		{
		"item_name":"ammunition",
		"item_quantity":10
		}
	]
}
```
### Example:
 <img src="https://raw.githubusercontent.com/devlucasrocha/markdown_images/master/postman%20prints/survivors/add_survivor.PNG" width="700">
 
---
 &nbsp;
 ## [Show all survivors and their items] - GET ```/api/v1/survivors```
 
 ### Example:
 
 <img src="https://raw.githubusercontent.com/devlucasrocha/markdown_images/master/postman%20prints/survivors/survivor_index.PNG" width="700">
 
 ---
 
 &nbsp;
 
 ## [Update survivor location] - PUT ```/api/v1/survivor/update_location```
 
 

 
 ###### headers
 ```Content-Type	application/json```
 
 ##### body
 
 ```JSON
{
	"survivor": {
		"id":1,
		"latitude":999999.312,
		"longitude":123135.41
	}
}
 ```
  ### Example:

 <img src="https://raw.githubusercontent.com/devlucasrocha/markdown_images/master/postman%20prints/survivors/update_location.PNG" width="700">
 
  ---
  
 &nbsp;
 
 ## [Report survivor infected] -  PUT  ```/api/v1/survivor/report_infected```
 
 
 ###### headers
 ```Content-Type	application/json```
 
 ##### body
 
 ```JSON
{
	"delator_id":1,
	"survivor_id":2
}
 ```

 ### Example:

&nbsp;

#### Before get infected


<img src="https://raw.githubusercontent.com/devlucasrocha/markdown_images/master/postman%20prints/survivors/report_infected%20-%20Before.PNG" width="700">

&nbsp;

#### After get infected
<img src="https://raw.githubusercontent.com/devlucasrocha/markdown_images/master/postman%20prints/survivors/report_infected%20-%20After.PNG" width="700">

   ---
   &nbsp;
   &nbsp;
   &nbsp;
   ## REPORTS CONTROLLER
&nbsp;
## [Get percentage of survivors infected] - GET ```/api/v1/report/infected_percentage```
 
 Endpoint -> 
 
 ### Example:
  <img src="https://raw.githubusercontent.com/devlucasrocha/markdown_images/master/postman%20prints/reports/infected_percentage.PNG" width="700">

---
&nbsp;

## [Get percentage of survivors non-infected] - GET ```/api/v1/report/uninfected_percentage```
 
 ### Example:
  <img src="https://raw.githubusercontent.com/devlucasrocha/markdown_images/master/postman%20prints/reports/uninfected_percentage.PNG" width="700">
  
---
&nbsp;

## [Get points lost of a infected survivor] - GET  ```/api/v1/report/points_lost```
 
 ### Example:
  <img src="https://raw.githubusercontent.com/devlucasrocha/markdown_images/master/postman%20prints/reports/points_lost.PNG" width="700">
 
---
&nbsp;
## [Get average of alive survivors] - GET ```/api/v1/report/average_resources```

  ### Example:
 <img src="https://raw.githubusercontent.com/devlucasrocha/markdown_images/master/postman%20prints/reports/average_resources.PNG" width="700">
 
---
&nbsp;
&nbsp;
## TRADES CONTROLLER
&nbsp;

## [Trade items with another survivor] - PUT   ```/api/v1/trade/trade_item```
 
 ###### headers
 ```Content-Type	application/json```
 
 ##### body
 
 ```JSON
 {
	"sender": {
	"sender_id": 2,
	"items": [
			{ "name": "food", "quantity": 1},
			{ "name": "ammunition", "quantity": 1}
		]
	},
	"receiver": {
	"receiver_id": 9,
	"items": [
			{ "name": "water", "quantity": 1},
			{ "name": "medication", "quantity": 6},
			{ "name": "food", "quantity": 2}
		]
	}
}
```

 
 ### Example:
<img src="https://raw.githubusercontent.com/devlucasrocha/markdown_images/master/postman%20prints/trades/trade_item.PNG" width="700">

&nbsp;

---
