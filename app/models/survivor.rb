class Survivor < ApplicationRecord
  has_many :items
  validates_presence_of :name, :gender, :age, :latitude, :longitude
  validates :age, numericality: { greater_than: 0, lower_than: 100 }
  enum infection_status: { uninfected: 0, infected: 1 }

  def survivor_title
    "#{self.name} #{self.age} #{self.infection_status}"
  end
end